CREATE DATABASE resto;
USE resto;

CREATE TABLE Serveur(
    idServeur integer not null auto_increment primary key,
    login varchar(50),
    password varchar(30)
);

CREATE TABLE CategPlat(
    idCategorie integer not null auto_increment,
    nomCateg varchar(20),
    primary key(idCategorie)
);

CREATE TABLE Plat(
    idPlat integer not null auto_increment primary key,
    idCategorie integer,
    nomPlat varchar(50),
    prix integer,
    image varchar(50),
    foreign key (idCategorie) references CategPlat(idCategorie)
);

CREATE TABLE TableResto(
    idTable integer not null auto_increment,
    numTable varchar(5),
    etat integer,
    image varchar(50),
    primary key(idTable)
);

CREATE TABLE Commande(
    idCommande integer not null auto_increment,
    idTable integer,
    idPlat integer,
    nombre integer,
    primary key(idCommande),
    foreign key (idTable) references TableResto(idTable),
    foreign key (idPlat) references Plat(idPlat)
);

CREATE TABLE Facture(
    idFacture integer not null auto_increment,
    idTable integer,
    montant decimal,
    primary key(idFacture),
    foreign key (idTable) references TableResto(idTable)
);


INSERT INTO Serveur (idServeur,login,password) VALUES (null,'Mitia','1234');
INSERT INTO Serveur (idServeur,login,password) VALUES (null, 'Taniah', '0000');

INSERT INTO CategPlat(idCategorie,nomCateg) VALUES (null,'Entree');
INSERT INTO CategPlat(idCategorie,nomCateg) VALUES (null,'Resistance');
INSERT INTO CategPlat(idCategorie,nomCateg) VALUES (null,'Dessert');

INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'2','Carbonara', 5000, 'assets/images/carbonara.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'2', 'Blanquette', 2500, 'assets/images/blanquette.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'3', 'BananaSplit', 3000, 'assets/images/bananasplit.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'2', 'Bouillabaisse', 3500, 'assets/images/bouillabaisse.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'1', 'Salade', 2000, 'assets/images/salade.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'2', 'Magret', 5000, 'assets/images/magret.jpg');

INSERT INTO TableResto(idTable,numTable,etat,image) VALUES (null, '1','1','assets/images/num1.jpg');
INSERT INTO TableResto(idTable,numTable,etat,image) VALUES (null, '2','10','assets/images/num2.jpg');
INSERT INTO TableResto(idTable,numTable,etat,image) VALUES (null, '3','1','assets/images/num3.jpg');
INSERT INTO TableResto(idTable,numTable,etat,image) VALUES (null, '4','10','assets/images/num4.jpg');
INSERT INTO TableResto(idTable,numTable,etat,image) VALUES (null, '5','1','assets/images/num5.jpg');

