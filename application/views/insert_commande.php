<!DOCTYPE html>
<html>
<head>
	<title>Accueil</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/table.css">
	<!------ Include the above in your HEAD tag ---------->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
</head>
<body>
	<section class="menu_list mt-60 mb-60">
	 <div class="container">
		<div class="row">
		   <div class="col-xl-12">
			  <div class="section-title text-center mb-60">
				 <p>Insert Command </p>
                 <h4><?php echo $id ; ?></h4>
              </div>
              <div>
                  <?php 
                        if(isset($message)){
                            echo "<h2>".$message."</h2>";
                        }
                        else{
                            echo "";
                        }
                  ?>
              <form  method="post" action="<?php echo base_url(); ?>index.php/command_controller/inserer_commande">
                <label>Id Table</label>
                <input type="text" name="idTable" value="<?php echo $id ; ?>" >
                <label>Id Plat</label>
                <input type="text" name="idPlat" >
                <label>Quantite</label>
                <input type="text" name="quantite" >
                <input type="submit" name="valider" value="Submit">
                </form>
            </div>
		   </div>
        </div>
    </div>
  </section>
</body>
</html>
