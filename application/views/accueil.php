<!DOCTYPE html>
<html>
<head>
	<title>Accueil</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/table.css">
	<!------ Include the above in your HEAD tag ---------->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
</head>
<body>
	<section class="menu_list mt-60 mb-60">
	 <div class="container">
		<div class="row">
		   <div class="col-xl-12">
			  <div class="section-title text-center mb-60">
				 <p>Famous for good food</p>
				 <h4>our menu</h4>
			  </div>
		   </div>
		</div>
		<div class="row center">
				<form  method="post" action="<?php echo base_url(); ?>index.php/command_controller/inserer_commande">
					<label>Recherche</label>
					<input type="text" name="recherche"  >
					<input type="submit" name="valider" value="Search">
                </form>
		   <!-- <ul class="nav nav-tabs menu_tab" id="myTab" role="tablist">
			  <li class="nav-item">
				 <a class="nav-link" id="breakfast-tab" data-toggle="tab" href="#breakfast" role="tab" aria-selected="false">Breakfast</a>
			  </li>
			  <li class="nav-item">
				 <a class="nav-link" id="lunch-tab" data-toggle="tab" href="#lunch" role="tab" aria-selected="false">Lunch</a>
			  </li>
			  <li class="nav-item">
				 <a class="nav-link active show" id="dinner-tab" data-toggle="tab" href="#dinner" role="tab" aria-selected="true">Dinner</a>
			  </li>
		   </ul> -->
		</div>
		<div class="row">
		   <div class="tab-content col-xl-12" id="myTabContent">
			  <div class="tab-pane fade active show" id="dinner" role="tabpanel" aria-labelledby="dinner-tab">
				 <div class="row">
                    <?php foreach($liste as $plat) { ?>
                        <div class="col-md-6">  
                            <div class="single_menu_list">
								<img src="<?php echo base_url(); ?><?php echo $plat->image; ?>">
                                <!-- <img src="<?php //echo base_url(); ?>assets/images/magret.jpg" alt=""> -->
                                <div class="menu_content">
                                    <h4><?php echo $plat->nomPlat; ?>  <span><?php echo "Ar ".$plat->prix; ?></span></h4>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
				 </div>
			  </div>
		   </div>
        </div>
        <div class="row">
		   <div class="col-xl-12">
			  <div class="section-title text-center mb-60">
				 <h4>Tables</h4>
			  </div>
		   </div>
		</div>
        <div class="row">
		   <div class="tab-content col-xl-12" id="myTabContent">
			  <div class="tab-pane fade active show" id="dinner" role="tabpanel" aria-labelledby="dinner-tab">
				 <div class="row">
                    <?php foreach($listeTable as $table) { ?>
                        <div class="col-md-6">  
                            <div class="single_menu_list">
							<a href="<?php echo base_url();?>index.php/command_controller/commande/?idTable=<?php echo $table->idTable; ?>">  <img src="<?php echo base_url(); ?><?php echo $table->image; ?>" alt=""></a>
                                <div class="menu_content">
                                    <h4><a href="<?php echo base_url();?>index.php/command_controller/commande/?idTable=<?php echo $table->idTable; ?>"><?php echo "Table  ".$table->numTable; ?></a></h4>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
				 </div>
			  </div>
		   </div>
		</div>
	 </div>
  </section>
</body>
</html>
