<html>
    <head>
        <title>Template</title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.min.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/table.css">
        <!------ Include the above in your HEAD tag ---------->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <!-- La vue s'affichera ici -->
                <?php echo $page; ?>
        </div>
    </body>
</html>