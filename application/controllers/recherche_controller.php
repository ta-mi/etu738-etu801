<?php 
    class recherche_controller extends CI_Controller {
        public function __construct() {
            parent::__construct();
            $this->load->database();
            // Load form helper library
            $this->load->helper('form');

            $this->load->helper('url');

            // Load form validation library
            $this->load->library('form_validation');

            // Load database
            $this->load->model('Commande_Model');
            $this->load->model('Plat_Model');
        }

        public function recherche() {
            $recherche = $_GET['recherche'];
            $data['liste'] =  $this->recherche_model->recherche($recherche);
            $page = $this->load->view('accueil', $data, true);
            $this->load->view('template', array('page' => $page));
        }
    }
?>
