<?php
    class Accueil_controller extends CI_Controller {
        public function __construct() {
            parent::__construct();
            // Load database
            $this->load->model('Plat_Model');
        }

        public function getListPlat() {
            $data['liste'] = $this->Plat_Model->getAllPlat();
            $page = $this->load->view('accueil', $data, true);
            $this->load->view('template', array('page' => $page));
            // $this->load->view('accueil' , $data);
        }

     public function home()
        {
            echo "Welcome";
        }
    }  
?>