<?php
    class Login extends CI_Controller {

        public function __construct() {
            parent::__construct();
            // Load form helper library
            $this->load->helper('form');

            $this->load->helper('url');

            // Load form validation library
            $this->load->library('form_validation');

            // Load database
            $this->load->model('Serveur_Model');
            $this->load->model('Plat_Model');
            $this->load->model('TableResto_Model');
        }

        public function validation_login() {
            // $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
            // $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
            $data = array(
                'login' => $this->input->post('login'),
                'password' => $this->input->post('password')
                );
                $result = $this->Serveur_Model->checkLogin($data);

            if ($result == TRUE) {
                $data['liste'] = $this->Plat_Model->getAllPlat();
                $data['listeTable'] = $this->TableResto_Model->getAllTable();
                $page = $this->load->view('accueil', $data, true);
                $this->load->view('template', array('page' => $page));
                // $this->load->view('test',$data);
            } else {
                $data['error'] = "Login or Passwod invalid";
                $this->load->view('loginTemplate' , $data);
            }
        }

        public function index()
        {
            $this->load->view('loginTemplate');
        }
    }
?>