<?php
    class command_controller extends CI_Controller {

        public function __construct() {
            parent::__construct();
            // Load form helper library
            $this->load->helper('form');

            $this->load->helper('url');

            // Load form validation library
            $this->load->library('form_validation');

            // Load database
            $this->load->model('Commande_Model');
        }


        public function commande() {
            $data['id'] = $_GET['idTable'];
            $page = $this->load->view('insert_commande', $data, true);
            $this->load->view('template', array('page' => $page));
        }

        public function inserer_commande() {
            $tab = array(
                'idCommande' => null,
                'idTable' =>  $this->input->post('idTable'),
                'idPlat' => $this->input->post('idPlat'),
                'nombre' => $this->input->post('quantite')
            );
            $this->Commande_Model->insert($tab);
            $data['message'] = "Data inserted successfully";
            $this->load->view('insert_commande', $data);
        }
    }
?>