<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    class TableResto{
        public $idTable;
        public $numTable;
        public $etat;
        public $image;

        public function __construct($idTable, $numTable, $etat,$image) {
                $this->idTable = $idTable;
                $this->numTable = $numTable;
                $this->etat = $etat;
                $this->image = $image;
            }
        public function __toString() {
            return $this->numTable;
        }
}
?>