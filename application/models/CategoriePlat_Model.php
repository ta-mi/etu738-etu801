<?php 
   class CategoriePlat_Model extends CI_Model {
    public $idCategoriePlat;

    public function __construct() { 
        parent::__construct(); 
    } 

    public function getAllCategoriePlat(){
        $response = array();
        // Select record
        $this->db->select('*');
        $query = $this->db->get('CategoriePlat');
        $response = $query->result_array();

        return $response;
    }

    public function insert($data) { 
        if ($this->db->insert("CategoriePlat", $data)) { 
            return true; 
        } 
    } 

    public function delete($idCategoriePlat) { 
        if ($this->db->delete("CategoriePlat", "idCategoriePlat= ".$idCategoriePlat)) { 
            return true; 
        } 
      } 
   
    public function update($data,$idCategoriePlat) { 
        $this->db->set($data); 
        $this->db->where("idCategoriePlat", $idCategoriePlat); 
        $this->db->update("CategoriePlat", $data); 
    } 

   } 
?>