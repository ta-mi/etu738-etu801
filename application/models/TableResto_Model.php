<?php 
    include('TableResto.php');
   class TableResto_Model extends CI_Model {
    public $idTableResto;

    public function __construct() { 
        parent::__construct(); 
    } 

    public function getAllTable(){
        $this->load->database();
        $response = array();
        $result = array();
        // Select record
        $this->db->select('*');
        $query = $this->db->get('TableResto');
        $response = $query->result_array();
        foreach($response as $row) {
            $result[] = new TableResto($row['idTable'],$row['numTable'],$row['etat'],$row['image']);
        }

        return $result;
    }

    public function insert($data) { 
        if ($this->db->insert("TableResto", $data)) { 
            return true; 
        } 
    } 

    public function delete($idTableResto) { 
        if ($this->db->delete("TableResto", "idTableResto= ".$idTableResto)) { 
            return true; 
        } 
    } 
   
    public function update($data,$idTableResto) { 
        $this->db->set($data); 
        $this->db->where("idTableResto", $idTableResto); 
        $this->db->update("TableResto", $data); 
    } 

   } 
?>