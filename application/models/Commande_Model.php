<?php 
   class Commande_Model extends CI_Model {
    public $idCommande;

    public function __construct() { 
        parent::__construct(); 
        $this->load->database();
    } 

    public function getAllCommande(){
        $response = array();
        // Select record
        $this->db->select('*');
        $query = $this->db->get('Commande');
        $response = $query->result_array();

        return $response;
    }

    public function insert($data) { 
        if ($this->db->insert("Commande", $data)) { 
            return true; 
        } 
    } 

    public function delete($idCommande) { 
        if ($this->db->delete("Commande", "idCommande= ".$idCommande)) { 
            return true; 
        } 
      } 
   
    public function update($data,$idCommande) { 
        $this->db->set($data); 
        $this->db->where("idCommande", $idCommande); 
        $this->db->update("Commande", $data); 
    } 

   } 
?>