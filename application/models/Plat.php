<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
    class Plat{
        public $idPlat;
        public $nomPlat;
        public $prix;
        public $image;
        public function __construct($idPlat, $nomPlat, $prix, $image) {
                $this->idPlat = $idPlat;
                $this->nomPlat = $nomPlat;
                $this->prix = $prix;
                $this->image = $image;
            }
        public function __toString() {
            return $this->nomPlat;
        }
}
?>