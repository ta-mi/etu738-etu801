<?php 
    include('Plat.php');
   class Plat_Model extends CI_Model {
    public $idPlat;

    public function __construct() { 
        parent::__construct(); 
    } 

    public function getAllPlat(){
        $this->load->database();
        $response = array();
        $result = array();
        // Select record
        $this->db->select('*');
        $query = $this->db->get('Plat');
        $response = $query->result_array();
        foreach($response as $row) {
            $result[] = new Plat($row['idPlat'],$row['nomPlat'],$row['prix'],$row['image']);
        }

        return $result;
    }

    public function insert($data) { 
        if ($this->db->insert("Plat", $data)) { 
            return true; 
        } 
    } 

    public function delete($idPlat) { 
        if ($this->db->delete("Plat", "idPlat= ".$idPlat)) { 
            return true; 
        } 
      } 
   
    public function update($data,$idPlat) { 
        $this->db->set($data); 
        $this->db->where("idPlat", $idPlat); 
        $this->db->update("Plat", $data); 
    } 

   } 
?>