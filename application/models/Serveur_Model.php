<?php 
   class Serveur_Model extends CI_Model {
	
      function __construct() { 
         parent::__construct(); 
      } 

      public function checkLogin($data){
         $condition = "login =" . "'" . $data['login'] . "' AND " . "password =" . "'" . $data['password'] . "'";
         $this->load->database();
         $this->db->select('*');
         $this->db->from('serveur');
         $this->db->where($condition);
         $this->db->limit(1);
         $query = $this->db->get();

         if ($query->num_rows() == 1) {
            return true;
         } 
         else {
            return false;
         }
      }

      public function getAllServeur(){
        $response = array();
        // Select record
        $this->db->select('*');
        $query = $this->db->get('Serveur');
        $response = $query->result_array();
    
        return $response;
      }
   
      public function insert($data) { 
         if ($this->db->insert("Serveur", $data)) { 
            return true; 
         } 
      } 
   
      public function delete($idServeur) { 
         if ($this->db->delete("Serveur", "idServeur= ".$idServeur)) { 
            return true; 
         } 
      } 
   
      public function update($data,$idServeur) { 
         $this->db->set($data); 
         $this->db->where("idServeur", $idServeur); 
         $this->db->update("Serveur", $data); 
      } 

   } 
?>