<?php 
   class Facture_Model extends CI_Model {
    public $idFacture;

    public function __construct() { 
        parent::__construct(); 
    } 

    public function getAllFacture(){
        $response = array();
        // Select record
        $this->db->select('*');
        $query = $this->db->get('Facture');
        $response = $query->result_array();

        return $response;
    }

    public function insert($data) { 
        if ($this->db->insert("Facture", $data)) { 
            return true; 
        } 
    } 

    public function delete($idFacture) { 
        if ($this->db->delete("Facture", "idFacture= ".$idFacture)) { 
            return true; 
        } 
    } 
   
    public function update($data,$idFacture) { 
        $this->db->set($data); 
        $this->db->where("idFacture", $idFacture); 
        $this->db->update("Facture", $data); 
    } 

   } 
?>