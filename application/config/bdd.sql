CREATE DATABASE restaurant;
USE resto;

CREATE TABLE Serveur(
    idServeur integer not null auto_increment primary key,
    login varchar(50),
    password varchar(30)
);

CREATE TABLE CategPlat(
    idCategorie integer not null auto_increment,
    nomCateg varchar(20),
    primary key(idCategorie)
);

CREATE TABLE Plat(
    idPlat integer not null auto_increment primary key,
    idCategorie integer,
    nomPlat varchar(50),
    prix integer,
    image varchar(10),
    foreign key (idCategorie) references CategPlat(idCategorie)
);

CREATE TABLE TableResto(
    idTable integer not null auto_increment,
    numTable varchar(5),
    etat integer,
    primary key(idTable)
);

CREATE TABLE Commande(
    idCommande integer not null auto_increment,
    idTable integer,
    idPlat integer,
    nombre integer,
    primary key(idCommande),
    foreign key (idTable) references TableResto(idTable),
    foreign key (idPlat) references Plat(idPlat)
);

CREATE TABLE Facture(
    idFacture integer not null auto_increment,
    idTable integer,
    montant decimal,
    primary key(idFacture),
    foreign key (idTable) references TableResto(idTable)
);


INSERT INTO Serveur (idServeur,login,password) VALUES (null,'Mitia','1234');
INSERT INTO Serveur (idServeur,login,password) VALUES (null, 'Taniah', '0000');

INSERT INTO CategPlat(idCategorie,nomCateg) VALUES (null,'Entree');
INSERT INTO CategPlat(idCategorie,nomCateg) VALUES (null,'Resistance');
INSERT INTO CategPlat(idCategorie,nomCateg) VALUES (null,'Dessert');

INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'2','Carbonara', 5000, 'carbonara.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'2', 'Blanquette', 2500, 'blanquette.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'3', 'BananaSplit', 3000, 'bananasplit.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'2', 'Bouillabaisse', 3500, 'bouillabaisse.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'1', 'Salade', 2000, 'salade.jpg');
INSERT INTO Plat (idPlat,idCategorie,nomPlat,prix,image) VALUES (null,'2', 'Magret', 5000, 'magret.jpg');

INSERT INTO TableResto(idTable,numTable,etat) VALUES (null, '1','1');
INSERT INTO TableResto(idTable,numTable,etat) VALUES (null, '2','10');
INSERT INTO TableResto(idTable,numTable,etat) VALUES (null, '3','1');
INSERT INTO TableResto(idTable,numTable,etat) VALUES (null, '4','10');
INSERT INTO TableResto(idTable,numTable,etat) VALUES (null, '5','1');

INSERT INTO Commande(idCommande,idTable,idPlat,nombre) VALUES (null, '', '', );
INSERT INTO Commande(idCommande,idTable,idPlat,nombre) VALUES (null, '', '', );
INSERT INTO Commande(idCommande,idTable,idPlat,nombre) VALUES (null, '', '', );

INSERT INTO Facture (idFacture, idTable,montant) VALUES (null,'',);